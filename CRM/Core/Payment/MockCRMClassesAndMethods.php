<?php
/**
 * Description
 *
 * @package TrustCommerce
 * @since 1.0.0
 * @author Kofi Oghenerukevwe Henrietta
 * @link http://github.com/rukykf
 *
 */

// Some mock functions

function ts($argument)
{
    return $argument;
}



$mockData = [];
$_SERVER['REMOTE_ADDR'] = "mock address";
$_SERVER['HTTP_USER_AGENT'] = "mock agent";

function tclink_send($data){
    global $mockData;
    return $mockData["tclink"];
}

function is_extension_loaded($extensionName){
    global $mockData;
    return $mockData["extension_loaded"];
}

class CRM_Core_Payment
{

    public static $singleton = null;

    public static function getSingleton(){
        if(self::$singleton !== null){
            return self::$singleton;
        }else{
            self::$singleton = Mockery::mock(CRM_Core_Payment::class);
            return self::$singleton;
        }
    }
    // mock properties and functions
    public $logging_level;

    public $_paymentProcessor = "";

    public $_processorName = "";

    public $xmlSafe = false;
}


class CRM_Core_Config
{
    public static function singleton()
    {
        return "mock config";
    }
}

class CRM_Utils_Array{
    public static function value($arg, $params){
        global $mockData;
        return $mockData["mock_value"][$arg];
    }
}

class CRM_Utils_Hook{
    public static function alterPaymentProcessorParams($paymentProcessor, &$params, $paymentParams){
        $params = $paymentParams;
    }
}

class CRM_Core_DAO{
    public static function setFieldValue($args){}

    public static function executeQuery(){
        global $mockData;
        return $mockData["execute_query"];
    }
}

class CRM_Core_Error{
    public static function singleton(){
        return new CRM_Core_Error();
    }

    public function push($errorCode, $secondCode, $data, $errorMessage){
        global $mockData;
        $mockData["error"] = [
            "error_message" => $errorMessage,
            "error_code" => $errorCode
        ];
    }
}

class CRM_Contribute_DAO_Contribution{
    public $invoice_id;

    public function find(){
        global $mockData;
        return $mockData["mock_check_duplicate"];
    }
}