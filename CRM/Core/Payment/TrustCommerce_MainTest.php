<?php
/**
 * Description
 *
 * @package ${NAMESPACE}
 * @since 1.0.0
 * @author Kofi Oghenerukevwe Henrietta
 * @link http://github.com/rukykf
 * @license GPL-2.0+
 *
 */

require_once 'CRM/Core/Payment/TrustCommerce.php';

use PHPUnit\Framework\TestCase;

$trustcommerce_params = [
    "total_amount"       => 2000,
    "amount" => 2000,
    "street_address"     => "mock street address",
    "postal_code"        => "mock_postal_code",
    "country"            => "840",
    "billing_first_name" => "first name",
    "billing_last_name"  => "last name",
    "invoiceID"          => "xxxxxxxxxxxxxxxxxxxx",
    "credit_card_number" => 1111111111111111,
    "cvv2"               => 111,
    "month"              => "january",
    "year"               => "2000",
    "contributionRecurID" => "123456",
    "is_recur" => 0,
    "subscriptionId" => "xxxx",
    "first_name" => "first name",
    "last_name" => "last name",
    "credit_card_exp_date" => [
        "Y" => '2020',
        "M" => 'Aug'
    ],
    "user_name" => "mock name",
    "password" => "mock password",
    "installments" => "installments"
];


class CRM_Core_Payment_TrustCommerce_DoDirectPayment_Test extends TestCase
{

    public static $mockPaymentProcessor = [
        'user_name' => 'a username',
        'password'  => 'a password',
        'name'      => 'a name'
    ];

    public static $trustCommerceInstance = null;
    public static $mock_crm_core_config;
    public static $mock_crm_utils_array;
    public static $mock_crm_utils_hook;
    public static $mock_crm_core_dao;
    public static $mock_crm_core_error;
    public static $mock_crm_contribute_dao_contribution;

    public static function setUpBeforeClass()
    {
        self::$mock_crm_core_config                 = Mockery::mock('crm_core_config');
        self::$mock_crm_utils_array                 = Mockery::mock('crm_utils_array');
        self::$mock_crm_utils_hook                  = Mockery::mock('crm_utils_hook');
        self::$mock_crm_core_dao                    = Mockery::mock('crm_core_dao');
        self::$mock_crm_core_error                  = Mockery::mock('crm_core_error');
        self::$mock_crm_contribute_dao_contribution = Mockery::mock("crm_contribute");

        self::$trustCommerceInstance = CRM_Core_Payment_TrustCommerce::singleton("test", self::$mockPaymentProcessor,
            self::$mock_crm_core_config,
            self::$mock_crm_utils_array, self::$mock_crm_utils_hook, self::$mock_crm_core_dao,
            self::$mock_crm_core_error, self::$mock_crm_contribute_dao_contribution);
        parent::setUpBeforeClass();
    }

    public function setUp()
    {
        // setup some default mock data that can be overridden by individual tests

        global $mockData;
        $mockData["extension_loaded"] = true;
        $mockData["tclink"]           = [
            'status'  => 'approve',
            'transid' => 'xxxxxxxxxxxxxxxx',
            'billingid' => '123456'
        ];
        parent::setUp();
    }

    function test_doDirectPayment_ReturnsError_WithMissingTCLinkExtension()
    {
        global $mockData;
        $mockData["extension_loaded"] = false;
        $params                       = [];
        self::$mock_crm_core_error->shouldReceive('push')->times(1)->with(9001, 0, null,
            "TrustCommerce requires that the tclink module is loaded")->andReturnSelf();
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals($response, self::$mock_crm_core_error);
    }

    function test_doDirectPayment_SetsCorrectParameters_WithRecurringPayments()
    {
        $params = [];

        self::$mock_crm_utils_array->shouldReceive('value')->times(1)->withSomeOfArgs("frequency_interval")->andReturn(15);
        self::$mock_crm_utils_array->shouldReceive('value')->times(1)->withSomeOfArgs("frequency_unit")->andReturn('day');
        self::$mock_crm_utils_array->shouldReceive('value')->times(1)->withSomeOfArgs("is_recur")->andReturn(1);
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(false);

        // mocking out the checks made by _isBlacklisted to CRM_Core_DAO
        self::$mock_crm_core_dao->shouldReceive('fetch')->times(2)->andReturn(false);
        self::$mock_crm_core_dao->shouldReceive('executeQuery')->times(2)->andReturnSelf();

        // act and assert
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals('1d', $response['cycle']);
        $this->assertEquals(15, $response['payments']);
        $this->assertEquals('store', $response['action']);

    }

    function test_doDirectPayment_ReturnsError_WithDuplicateTransaction()
    {
        $params = [];

        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(true);
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_core_error->shouldReceive('push')->times(1)->with(9004, 0, null,
            'It appears that this transaction is a duplicate.  Have you already submitted the form once?  If so there may have been a connection problem. You can try your transaction again.  If you continue to have problems please contact the site administrator.')->andReturnSelf();
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals($response, self::$mock_crm_core_error);

    }

    function test_doDirectPayment_ReturnsError_WhenOnIPBlacklist()
    {
        $params = [];

        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(false);
        self::$mock_crm_core_error->shouldReceive('push')->times(1)->with(9009, 0, null,
            "Your transaction was declined. Please check the correctness of your credit card information, including CC number, expiration date and CVV code.")->andReturnSelf();

        // mocking out the checks made by _isBlacklisted to CRM_Core_DAO
        self::$mock_crm_core_dao->shouldReceive('fetch')->times(1)->andReturn(true);
        self::$mock_crm_core_dao->ip = "mock address";
        self::$mock_crm_core_dao->id = "mock id";
        self::$mock_crm_core_dao->shouldReceive('executeQuery')->times(1)->with('SELECT * FROM `trustcommerce_blacklist`')->andReturnSelf();

        // act and assert
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals(self::$mock_crm_core_error, $response);

    }

    function test_doDirectPayment_ReturnsError_WhenOnAgentBlacklist()
    {
        $params = [];

        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(false);
        self::$mock_crm_core_error->shouldReceive('push')->times(1)->with(9009, 0, null,
            "Your transaction was declined. Please check the correctness of your credit card information, including CC number, expiration date and CVV code.")->andReturnSelf();

        // mocking out the checks made by _isBlacklisted to CRM_Core_DAO
        self::$mock_crm_core_dao->ip = "mock address";
        self::$mock_crm_core_dao->id = "mock id";
        self::$mock_crm_core_dao->shouldReceive('executeQuery')->times(2)->andReturnSelf();
        // the first call to this method checks the ipBlacklist, second call checks the agentBlacklist and should return true
        self::$mock_crm_core_dao->shouldReceive('fetch')->times(1)->andReturn([false, true]);

        // act and assert
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals(self::$mock_crm_core_error, $response);
    }

    function test_doDirectPayment_ReturnsError_WithBlacklistedParams()
    {
        $params = [];
        global $trustcommerce_params;
        $trustcommerce_params['country'] = 'BR';
        $trustcommerce_params['total_amount'] = 12;

        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(false);
        self::$mock_crm_core_error->shouldReceive('push')->times(1)->with(9009, 0, null,
            "Your transaction was declined. Please check the correctness of your credit card information, including CC number, expiration date and CVV code.")->andReturnSelf();

        // mocking out the checks made by _isBlacklisted to CRM_Core_DAO
        self::$mock_crm_core_dao->ip = "mock address";
        self::$mock_crm_core_dao->id = "mock id";
        self::$mock_crm_core_dao->shouldReceive('fetch')->times(2)->andReturn(false);
        self::$mock_crm_core_dao->shouldReceive('executeQuery')->times(2)->andReturnSelf();

        // act and assert
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals(self::$mock_crm_core_error, $response);

        $trustcommerce_params['country'] = '840';
        $trustcommerce_params['total_amount'] = 2000;

    }

    function test_doDirectPayment_ReturnsTCParams_OnSuccessfulPaymentWithValidData()
    {
        $params = [];

        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_contribute_dao_contribution->shouldReceive('find')->times(1)->andReturn(false);

        // mocking out the checks made by _isBlacklisted to CRM_Core_DAO
        self::$mock_crm_core_dao->shouldReceive('fetch')->times(2)->andReturn(false);
        self::$mock_crm_core_dao->shouldReceive('executeQuery')->times(2)->andReturnSelf();

        // act and assert
        $response = self::$trustCommerceInstance->doDirectPayment($params);
        $this->assertEquals('xxxxxxxxxxxxxxxx', $response['trxn_id']);
        $this->assertEquals(2000, $response['gross_amount']);
        $this->assertEquals('123456', $response['recurr_profile_id']);

    }

    function test_updateSubscriptionBillingInfo_ReturnsTrue_WithValidData(){
        global $trustcommerce_params;
        $params = $trustcommerce_params;
        $message = '';

        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });

        $response = self::$trustCommerceInstance->updateSubscriptionBillingInfo($message, $params);
        $this->assertTrue($response);
    }

    public function test__setParam_ReturnsFalse_WithInvalidData()
    {
        $output = self::$trustCommerceInstance->_setParam("field", ["test", "value"]);
        $this->assertFalse($output);
    }

    public function test_checkConfig_ReturnsError_WithInvalidPaymentProcessorConfig(){
        self::$trustCommerceInstance->_paymentProcessor = [];
        $result = self::$trustCommerceInstance->checkConfig();
        $this->assertEquals("Customer ID is not set for this payment processor<p>Password is not set for this payment processor", $result);

        // clean things up for subsequent tests
        self::$trustCommerceInstance->_paymentProcessor = self::$mockPaymentProcessor;
    }

    public function test_checkConfig_ReturnsNull_WithValidPaymentProcessorConfig(){
        self::$trustCommerceInstance->_paymentProcessor = self::$mockPaymentProcessor;
        $result = self::$trustCommerceInstance->checkConfig();
        $this->assertNull($result);
    }

    public function test_cancelSubscription_ReturnsError_OnFailedRequest(){
        global $mockData;
        $mockData["tclink"] = [
            "status" => 'error'
        ];
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_core_error->shouldReceive('push')->zeroOrMoreTimes()->with(9002, 0, null,
            "Could not initiate connection to payment gateway")->andReturnSelf();
        $result = self::$trustCommerceInstance->cancelSubscription();
        $this->assertEquals(self::$mock_crm_core_error, $result);
    }

    public function test_cancelSubscription_ReturnsTrue_OnSuccessfulRequest(){
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        $result = self::$trustCommerceInstance->cancelSubscription();
        $this->assertTrue($result);
    }

    public function test_changeSubscriptionAmount_ReturnsFalse_OnFailedRequest(){
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            return $trustcommerce_params[$field];
        });
        self::$mock_crm_core_error->shouldReceive('push')->zeroOrMoreTimes()->with(9002, 0, null,
            "Could not initiate connection to payment gateway")->andReturnSelf();
        global $mockData;
        $mockData["tclink"] = [
            "status" => 'error'
        ];
        $result = self::$trustCommerceInstance->changeSubscriptionAmount();
        $this->assertFalse($result);
    }

    public function test_changeSubscriptionAmount_ReturnsTrue_OnSuccessfulRequest(){
        self::$mock_crm_utils_array->shouldReceive('value')->zeroOrMoreTimes()->withAnyArgs()->andReturnUsing(function (
            $field,
            $params = [],
            $data = ''
        ) {
            global $trustcommerce_params;

            if(array_key_exists($field, $trustcommerce_params)){
                return $trustcommerce_params[$field];
            }else{
                return "mock data";
            }

        });
        $result = self::$trustCommerceInstance->changeSubscriptionAmount();
        $this->assertTrue($result);
    }

    public function test_constructor_initializesNewObject_WithValidData()
    {
        $trustCommerceInstance = new CRM_Core_Payment_TrustCommerce("test", self::$mockPaymentProcessor,
            self::$mock_crm_core_config,
            self::$mock_crm_utils_array, self::$mock_crm_utils_hook, self::$mock_crm_core_dao,
            self::$mock_crm_core_dao, self::$mock_crm_contribute_dao_contribution);
        $this->assertEquals(self::$mockPaymentProcessor, $trustCommerceInstance->_paymentProcessor);
        $this->assertEquals("TrustCommerce", $trustCommerceInstance->_processorName);
        $this->assertEquals("$trustCommerceInstance->logging_level", 4);
    }

    public function test_singleton_ReturnsAnInstanceOfTrustCommerce_WithValidData()
    {
        $trustCommerceInstance = null;
        $trustCommerceInstance = CRM_Core_Payment_TrustCommerce::singleton("singleton_test",
            self::$mockPaymentProcessor, self::$mock_crm_core_config,
            self::$mock_crm_utils_array, self::$mock_crm_utils_hook, self::$mock_crm_core_dao,
            self::$mock_crm_core_dao, self::$mock_crm_contribute_dao_contribution);
        $this->assertNotNull($trustCommerceInstance);
        $this->assertInstanceOf(CRM_Core_Payment_TrustCommerce::class, $trustCommerceInstance);
    }


}