# org.fsf.payment.trustcommerce

This is a modified version of the TrustCommerce extension.

The original extension can be found and downloaded here: https://vcs.fsf.org/?p=trustcommerce.git;a=summary

Unlike the original extension, this version does not require the CiviCRM to run. 
It can be executed using the PHP CLI from the repository's root directory.


This extension allows CiviCRM users to use TrustCommerce.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP 7.0 or greater
* Composer (To install PHPUnit and run the unit tests)
* CiviCRM 4.7.x

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

To execute this extension, clone this repository and run this command

 `php CRM/Core/Payment/TrustCommerce.php`

## Installation

Intall the module in your custom extensions directory. The name of the top
level directory unpacked by the tarball should be changed to
`org.fsf.payment.trustcommerce`.

You will need to add rows to `civicrm_payment_processor_type` in your database.

    | id | name          | title                                         | description                     | is_active | is_default | user_name_label | password_label | signature_label | subject_label | class_name            | url_site_default                       | url_api_default | url_recur_default                      | url_button_default | url_site_test_default                  | url_api_test_default                   | url_recur_test_default                 | url_button_test_default | billing_mode | is_recur | payment_type | payment_instrument_id |
    | 19 | TrustCommerce | TrustCommerce (org.fsf.payment.trustcommerce) | TrustCommerce Payment Processor |         1 |       NULL | Vendor ID       | Password       |                 |               | Payment_TrustCommerce | https://vault.trustcommerce.com/trans/ |                 | https://vault.trustcommerce.com/trans/ |                    | https://vault.trustcommerce.com/trans/ | https://vault.trustcommerce.com/trans/ | https://vault.trustcommerce.com/trans/ |                         |            1 |        1 |            1 |                     1 |

## Run Tests
To run the tests install the PHPUnit dependency with Composer (ensure Composer is available on your machine)

`composer install`

Then run the default test suite with this command (from the repository's root directory)

`./vendor/bin/phpunit`

The test files are currently located in `./CRM/Core/Payment`. The test files end with the suffix "Test.php"

## Usage

Go to Administer -> System Settings -> Payment Processors and add The
TrustCommerce PP.

## Known Issues

There are no hooks for installation and uninstallation of plugin, so tables
need to be updated manually.

If the class names in the `civicrm_payment_processor_type` or
`civicrm_payment_processor` tables are incorrect, then they need to be changed
to `Payment_TrustCommerce`.

